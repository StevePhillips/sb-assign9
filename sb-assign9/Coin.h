// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Coin.h
//
// Represents a lockable coin object that prevents
// manipulation if the user hasnt unlocked the object
// with the correct key password
// -----------------------------
#import <Foundation/Foundation.h>
#import "Lockable.h"

#define Heads YES
#define Tails NO

@interface Coin : NSObject <Lockable>

@property BOOL face;
-(instancetype) init;
-(void) flip;
-(NSString*) description;

@end
