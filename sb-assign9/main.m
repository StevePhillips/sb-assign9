// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// main.m
//
// The real driver
// ---------------
#import "Driver.h"
#import "Driver2.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Driver* driver = [[Driver alloc] init];
        [driver run];
        
        Driver2* driver2 = [[Driver2 alloc] init];
        [driver2 run];
    }
    return 0;
}
