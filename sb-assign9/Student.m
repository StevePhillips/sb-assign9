// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Student.m
//
// Student object who can be copied without the copy
// effecting the original
// ----------------
#import "Student.h"

@implementation Student

-(id) copyWithZone:(NSZone *)zone
{
    Student* newstudent =
    [[Student alloc] initWithName:self.name andAge:self.age];
    
    // copy the grades then return the student
    for(NSNumber* n in self.grades)
    {
        [newstudent.grades addObject:[n copy]];
    }
    
    return newstudent;
}
-(instancetype) initWithName: (NSString*) n andAge: (int) g
{
    if(self = [super init])
    {
        _name = n;
        _age = g;
        
        _grades = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSString*) description
{
    NSString* str =
    [NSString stringWithFormat:@"%@: %i\n%@", _name, _age, _grades];
    return str;
}

@end
