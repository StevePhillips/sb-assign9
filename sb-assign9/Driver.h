// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Driver.h
//
// Runs the first problem: lockable coin
// -------------------------------
#import <Foundation/Foundation.h>

@interface Driver : NSObject

-(void) run;

@end
