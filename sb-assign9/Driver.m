// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Driver.m
//
// Runs the first problem: lockable coin
// ---------------
#import "Driver.h"
#import "Coin.h"

@implementation Driver

-(void) run
{
    Coin* coin = [[Coin alloc] init];
    
    [coin flip];
    
    NSLog(@"%@", coin);
    
    coin.face = Heads;
    
    [coin unlock:1111];
    
    if([coin setKey:1234] == YES)
        NSLog(@"Succesfully changed the key");
    
    [coin flip];
    
    NSLog(@"%@", coin);
    
    coin.face = Heads;
    
    NSLog(@"%@", coin);
    
    coin.face = Tails;
    
    NSLog(@"%@", coin);
}

@end
