// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Driver2.h
//
// The implementation of driver2 which
// shows that the copy functionality  of the student
// class works
// ----------------
#import "Driver2.h"
#import "Student.h"
@implementation Driver2

-(void) run
{
    NSLog(@"\nPart 2\n");
    Student* john = [[Student alloc] initWithName:@"John" andAge:21];
    
    [john.grades addObject:@100];
    [john.grades addObject:@70];
    [john.grades addObject:@89];
    
    Student* alice = [john copy];
    
    alice.name = @"Alice";
    alice.age = 23;
    
    [alice.grades addObject:@100];
    [alice.grades addObject:@73];
    
    // By displaying down here after the modifications we can see to be
    // sure that the objects can be modified without affecting eachother
    NSLog(@"%@", john);
    NSLog(@"%@", alice);
    
}

@end
