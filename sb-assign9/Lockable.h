// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Driver2.h
//
// A protocal that interfaces implement so that
// they can prevent outside forces from manipulating
// their objects
// ---------------
@protocol Lockable

-(BOOL) setKey: (int) nkey;
-(void) lock: (int) key;
-(void) unlock: (int) key;
-(BOOL) locked;

@end