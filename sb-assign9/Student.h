// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Student.h
//
// The student class must represent a student object
// that can have its contents copied. and the new copy
// does not have any effect on the original
// -----------------------------
#import <Foundation/Foundation.h>

@interface Student : NSObject <NSCopying>

@property NSString* name;
@property int age;
@property NSMutableArray* grades;

-(instancetype) initWithName: (NSString*) n andAge: (int) g;

-(NSString*) description;

@end
