// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Driver2.h
//
// Executes the solution to problem statement 2:
// Create a student class the implements NSCopying
// Protocal, make an object called john and copy
// a new object out of it called alice. ensure they
// can be manipulated without effecting eachother
// ------------------------------
#import <Foundation/Foundation.h>

@interface Driver2 : NSObject

-(void) run;

@end
