// -------------------------------------------------
// Author: Steve Bennett
// IMG240
//
// Coin.m
//
// Implementation of coin which can be locked
// -----------------
#import "Coin.h"

@implementation Coin
{
    int theKey;
    BOOL islocked;
}
@synthesize face = _face;
-(instancetype) init
{
    if(self = [super init])
    {
        //islocked = NO;
        //[self flip];
        islocked = YES;
        theKey = 1111;
    }
    return self;
}
-(void) puterr
{
    fputs("Operation attempted on locked object\n", stderr);
}
-(BOOL) setKey:(int)nkey
{
    if([self locked] == NO)
    {
        theKey = nkey;
        return YES;
    }
    else
    {
        [self puterr];
        return NO;
    }
}

-(void) lock:(int)key
{
    if(key == theKey)
        islocked = YES;
    else
        [self puterr];
}
-(void) unlock:(int)key
{
    if(key == theKey)
        islocked = NO;
    else
        [self puterr];
}

-(BOOL) locked
{
    return islocked;
}

-(void) flip
{
    if([self locked] == NO)
        self.face = rand()%2;
    else
        [self puterr];
}

-(BOOL) face
{
    if([self locked] == NO)
        return _face;
    else
        [self puterr];
    NSLog(@"face always returns tails if locked\n");
    return Tails;
}
-(void) setFace:(BOOL)face
{
    if([self locked] == NO)
        _face = face;
    else
        [self puterr];
}

-(NSString*) description
{
    if([self locked] == YES)
        return @"Cannot get description of coin when locked";
    
    else if(self.face == Heads)
        return @"Heads";
    
    else if(self.face == Tails)
        return @"Tails";
    
    return nil;
}

@end
